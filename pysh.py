

from os import getenv, chdir
from pathlib import Path
from itertools import chain
from subprocess import call

PATH = getenv('PATH').split(':')

bins = list(chain(*[[binary.name
                     for binary in Path(p).glob('*')]
                    for p in PATH]))

cwd = "."
while True:
    cmd = input('> ').strip()
    args = cmd.split(' ')
    if args[0] == "cd":
        chdir(args[1])
    elif args[0] in bins:
        try:
            run = ["/bin/bash", "-c", cmd]
            p = call(run)
        except:
            print()
    else:
        try:
            print(eval(cmd))
        except Exception as e:
            print(f"Python error: {e}")
